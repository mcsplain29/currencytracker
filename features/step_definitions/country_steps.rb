Given /the following countries exist:/ do |countries|
  countries.hashes.each do |country|
    country[:user_id] = @user.id
  end

  Country.create!(countries.hashes)
end

Then /^I should see the following table:$/ do |expected_table|
  document = Nokogiri::HTML(page.body)
  rows = document.css('section>table>tr').collect { |row| row.xpath('.//th|td').collect {|cell| cell.text.strip } }
  expected_table.diff!(rows)
end
