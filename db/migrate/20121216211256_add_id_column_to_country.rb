class AddIdColumnToCountry < ActiveRecord::Migration
  def up
    add_column :countries, :id, :primary_key

    add_index :countries, :code
  end
  def down 
    remove_coumn :countries, :id
  
    remove_index :coutnries, :code
  end


end
