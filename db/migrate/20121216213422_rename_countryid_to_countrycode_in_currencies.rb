class RenameCountryidToCountrycodeInCurrencies < ActiveRecord::Migration
  def up
    rename_column :currencies, :country_id, :country_code
    add_column :currencies, :country_id, :integer

    add_index :currencies, :country_code
  end

  def down
    rename_column :currencies, :country_code, :country_id
    remove_colum :currencies, :country_id

    remove_index :currencies, :country_id
  end
end
