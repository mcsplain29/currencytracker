class AddIdColumnToCurrencies < ActiveRecord::Migration
  def up
    add_column :currencies, :id, :primary_key

    add_index :currencies, :code
  end
  def down 
    remove_coumn :currencies, :id
  
    remove_index :currencies, :code
  end
end
