class Country < ActiveRecord::Base
  self.primary_key = :id
  attr_accessible :id, :name, :code, :visited, :user_id

  validates_presence_of :name
  validates_presence_of :code
  validates_uniqueness_of :code, :scope => :user_id, :allow_blank => true

  has_many :currencies

  belongs_to :user

  after_commit :create_visit

  accepts_nested_attributes_for :currencies, :allow_destroy => true

  scope :visited, :conditions => { :visited => true }
  scope :not_visited, :conditions => { :visited => false }

  def create_visit
    countryNumber = Country.visited.count
    Visit.create(:number => countryNumber)
  end
end
