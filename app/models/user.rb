class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me

  after_commit :seed

  has_many :countries

  def seed
    DataUpdater.update unless user_has_countries
  end

  def user_has_countries
    user_id = self.id
    !Country.find_by_user_id(user_id).nil?
  end
end
