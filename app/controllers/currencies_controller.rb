class CurrenciesController < ApplicationController
  before_filter :authenticate_user!

  # GET /currencies
  # GET /currencies.xml
  def index
    get_currencies
    @visits = Visit.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @currencies }
    end
  end

  def get_currencies
    @currencies = []

    current_user.countries.each do |country|
      country.currencies.each do |currency| 
        @currencies << currency
      end
    end
  end

  # GET /currencies/1
  # GET /currencies/1.xml
  def show
    get_currencies
    
    if Currency.find(params[:id]).country.user == current_user
      @currency = Currency.find(params[:id]) 

      respond_to do |format|
        format.html # show.html.erb
        format.xml  { render :xml => @currency }
      end
    else
      # Redirect for those who are not authorized
      redirect_to currencies_path
    end
  end

  def update
    @currency = Currency.find(params[:id])
    @country = @currency.country
    
    respond_to do |format|
      if @country.update_attributes(:visited => params[:currency][:collected])
        format.js   
        format.html { redirect_to(@country, :notice => 'Country was successfully updated.') }
        format.xml  { head :ok }
      else
        format.js
        format.html { render :action => "edit" }
        format.xml  { render :xml => @country.errors, :status => :unprocessable_entity }
      end
    end
  end
end
