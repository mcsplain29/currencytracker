$(document).ready(function() {
  edit_id = page_edit_id();

  $(edit_id + ' input[type=checkbox]').click(function() {      
    if (edit_id == '.edit_currency') {
      chart_selector = '#collected_currencies_chart img';
      visited = 'Collected';
      notVisited = 'Not Collected'
    } else {
      chart_selector = '#visited_countries_chart img';
      visited = 'Visited';
      notVisited = 'Not Visited'
    }

    $(this).parent('form').submit();
    text = $(this).parent('form').next().text();

    chart_url = $(chart_selector).attr('src');
    chart_visited = chart_url.split(':')[2].split('&')[0].split(',')[0];
    chart_unvisited = chart_url.split(':')[2].split('&')[0].split(',')[1];
    chart_url_left = 'http://chart.googleapis.com/chart?cht=p3&chs=300x100&chco=0000FF&chd=t:';
    chart_url_right = '&chdl=' + visited + '|' + notVisited;

    if (text == visited) {
      text = notVisited;

      chart_url = chart_url_left + --chart_visited + ',' + ++chart_unvisited + chart_url_right;
    } else {
      text = visited;
      chart_url = chart_url_left + ++chart_visited + ',' + --chart_unvisited + chart_url_right;
    }

    $(this).parent('form').next().text(text);
    $(chart_selector).attr('src', chart_url);

    checkDisplayedBoxes();

  });

  $('#search').bind('change keypress keydown keyup', function() {
    searchNow();
  });

  function searchNow() {
    searchFor = new RegExp($('#search').val(),'i');
    rows = $('tr')
    rows.splice(0,1) //Removes header row

    rows.each(function() {
      current = $(this).children().first().text();
      search = current.search(searchFor);
      if (search < 0) {
        $(this).css('display','none');
      } else {
        $(this).css('display','table-row');
      }
    });

    checkDisplayedBoxes();
  };

  function page_edit_id() {
    page = $('section h1').text();

    if (page == 'Currencies') {
      edit_id = '.edit_currency';
    } else {
      edit_id = '.edit_country';
    }
    return edit_id;
  }

  function checkDisplayedBoxes() {
    // This function will look for displayed boxes and adjust the visit_all box if all are checked
    rows = $('tr');
    rows.splice(0,1); //Removes header row

    areallchecked = true;

    rows.each(function() {
      checked = $(this).children().children(edit_id).children('input[type=checkbox]').prop('checked');
      displayed = $(this).css('display');

      if (checked != true && displayed != 'none') {
        areallchecked = false;
      }
    });

    if (areallchecked) {
      $('#visit_all').prop('checked',true);
    } else {
      $('#visit_all').prop('checked',false);
    }


  }
  checkDisplayedBoxes();

  $('#visit_all').bind('change', function() {
    rows = $('tr');
    rows.splice(0,1); //Removes header row

    areCheckedAlready = true;

    rows.each(function() {
      checked = $(this).children().children(edit_id).children('input[type=checkbox]').prop('checked');
      displayed = $(this).css('display');
      if (!checked  && displayed != 'none') {
        $(this).children().children(edit_id).children('input[type=checkbox]').each(function() {this.click()});
        areCheckedAlready = false;
      }
    });

    if (areCheckedAlready == true) {
      // All rows are currently checked, lets uncheck them all to be nice!
      rows.each(function() {
        displayed = $(this).css('display');    
        if (displayed != 'none') {
          $(this).children().children(edit_id).children('input[type=checkbox]').each(function() {this.click()});
        }

      });
    }
  });
});
