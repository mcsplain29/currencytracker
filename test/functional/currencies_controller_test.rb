require 'test_helper'

class CurrenciesControllerTest < ActionController::TestCase
  should_not_respond_to_actions :new => :get,
                                :destroy => :get,
                                :create => :post,
                                :edit => :get,
                                :update => :put

  setup do
    sign_in users(:one)
    @country = countries(:one)
    users(:one).countries << @country

    @currency1 = currencies(:one)
    @currency1.country_id = @country.id

    @controller = CurrenciesController.new
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:currencies)
  end

  test "should show currency" do
    get :show, :id => @currency1.to_param
    assert_response :success
  end
end
