CurrencyTracker
===============


Assumptions
-----------
*  When creating a new user, we would automatically seed that user with countries and currencies
*  When attempting to "Visit" a country, a user can check a box in the Visit column, which automatically updates the database, rather than requiring the user to hit a submit button, etc.
*  When attempting to "Visit" a country, the status of the visited or not visisted should automatically be updated on the page.
*  If a list is all checked, the "Check all" checkbox should reflect the same state. This should be dynamic, espeicially if a list being filtered.

With more time...
-------------
... I would have built a more robust test suite and tested the Javascript better.  Although the code works in most cases, there are times that sqlite3 gets overloaded (e.g. when checking all countries box) which is a database problem but could probably be solved by grouping the request in to a single request rather than individual requests.

The visit graphing would also be more elaborate and unique to country vs currency.

I also would have prefered separating out javascript functions into separate classes in the rails asset pipeline (and maybe even coffeescripting instead), but since the pipeline wasn't already built, I didn't take the time to do so.  Currently, if we were to add another page to the app, the javascript from Countries and Currencies would fire on those pages as well, which clearly is not acceptable for both performance and practicality.  

Thanks for the opportunity!


Instructions
------------

CurrencyTracker allows you to track your personal collection of world currencies, by tagging the countries that you've visited along your travels.

Setup
-----

Seed the database with currencies and countries by running:

```bash
rake db:seed
```

Testing
-------

Run tests with:

```bash
RAILS_ENV=test rake test
```

Cucumber features can be run with:

```bash
cucumber
```

Features
--------

* Track Visited Countries
* Track Collected Currencies
* Charts show you how far along you are!
